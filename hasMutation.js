const BASES = ['A', 'T', 'C', 'G'];
const MUTATIONS = ['AAAA', 'TTTT', 'CCCC', 'GGGG'];
const LENGTH_STRING = 4;
const TOTAL_MINIMAL_MUTATION = 2;

/**
 * Verifica si hay al menos 2 secuencias como MUTATIONS en el array
 * Si asi sucede retorna true, sino retorna false
 * @param {Array} dna 
 */
module.exports = function hasMutation(dna){
    var mutationCount = 0;

    // Validaciones del array pasos 1 y 2
    // Paso 1 verificar que el array sea de NxN
    if(!checkArray(dna))
        throw new Error("Array incorrecto, la longitud del array no coincide con la longitud de todos los elementos y/o esa longitud es menor que la secuencia de mutacion"); 

    // Paso 2 verificar letras validas (A,T,C,G)
    if(!checkString(dna))
        throw new Error("El array contiene caracteres invalidos");


    // Comprobacion de mutaciones pasos 3 en adelante
    // Paso 3 verificar si hay mas de 2 secuencias con 4 letras iguales en filas
    mutation = checkMutation(dna);
    
    if(mutation >= 2)
        return true;

        
    // Paso 4 Si no hubo, sigo ahora verificando con columnas para eso:
    //  Paso 4.1 armo columnas y mientras tanto verifico si hay mutacion y 
    //  si el total de mutaciones es 2 o +

    mutation = checkColumnMutation(dna, mutation);

    if(mutation >= 2)
        return true;


    // Paso 5 verifico mutaciones en diagonal para eso:
    //  Paso 5.1 armo diagonales validas
    //  Por cada diagonal hecha me fijo si hay mutacion y si hay, si se alcanza el numero
    // 2 o mas
    
    mutation = checkDiagonalMutation(dna, mutation);

    if(mutation >= 2)
        return true;


    return false;
}
/**
 * Verifica que el array sea de NxN
 * @param {Array} vector 
 */
function checkArray(vector){
    // Paso 1: verificar que el array no este vacio (muy necesario porque every 
    // de un array vacio da true) y que la longitud del array sea mayor o igual 
    // de 4 (secuencia de mutacion)
    if(vector.length == 0 || vector.length < LENGTH_STRING){
        return false;
    }
    //Paso 2: verificar que todos los elementos tengan la misma longitud y que esta
    //        a su vez sea igual a la longitud del vector
    return vector.every(item=>item.length);
}

/**
 * Verifica que en el array solo haya letras validas
 * las letras validas se toman de la constante BASES
 * @param {Array} vector 
 */
function checkString(vector){
    var toEvaluate = vector.join('').split('');
    
    var result = toEvaluate.some(item =>!BASES.includes(item));
 
    return !result;
}

/**
 * Verifica si en la secuencia (el string pasado como parametro)
 * se incluye alguna de las mutaciones establecidas en la 
 * constante array MUTATIONS
 * @param {String} sequence 
 */
function sequenceHasMutation(sequence){
    let hasMutation = false;
    let it = 0;

    while(hasMutation == false && it < MUTATIONS.length){
        if(sequence.includes(MUTATIONS[it]))
            hasMutation = true;
        
        it++;
    }

    return hasMutation;

}


/**
 * Chequea si existe mutacion en el vector
 * Sentido de filas
 * Retorna el total de mutaciones encontradas
 * @param {Array} vector 
 */
function checkMutation(vector){
    let totalMutations = 0;

    vector.forEach(item =>{
        if(sequenceHasMutation(item))
            totalMutations++;
    })

    return totalMutations;
}

/**
 * Recorre el vector por columnas y verifica si hay mutaciones
 * Agrega las mutaciones a las ya encontradas y corta la
 * ejecucion si el total de mutaciones llega al total
 * de mutaciones establecidas para determinar que efectivamente el dna
 * se encuentra mutado.
 * @param {Array} vector 
 * @param {Number} mutation 
 */
function checkColumnMutation(vector, mutation){
    let j = 0;
    let countMutation = mutation;
    let str;

    while(countMutation < TOTAL_MINIMAL_MUTATION && j < vector.length){
        str = '';

        for(let i = 0; i < vector.length; i++){
            str += vector[i][j];
        }

        if(sequenceHasMutation(str)){
            countMutation++
        }
        j++;
    }

    return countMutation;

}


/**
 * Se comienza con la generacion de las secuencias diagonales
 * y al mismo tiempo se chequea el limite de mutaciones.
 * Se verifica la llegada al limite de mutaciones despues de cada 
 * secuencia generada para optimizar los tiempos de respuesta.
 * 
 * @param {Array} dna 
 * @param {Number} mutation 
 */
function checkDiagonalMutation(dna, mutation){

    let countMutation = mutation;
    let i = 0;

    while(i < dna.length && countMutation < TOTAL_MINIMAL_MUTATION){
        // Este caso generaria la diagonal principal
        // Este caso genera las diagonales inferiores a la diagonal principal
        if(sequenceHasMutation(generateSequense(dna, i, 0, 1, 1, 1))){
            countMutation++;
            if(countMutation >= TOTAL_MINIMAL_MUTATION)
                return countMutation;
        }

        // Este caso genera diagonales superiores a la diagonal principal
        if(sequenceHasMutation(generateSequense(dna, 0, i+1, 1, 1, 1))){
            countMutation++;
            if(countMutation >= TOTAL_MINIMAL_MUTATION )
                return countMutation;
        }    

        // Este caso genera diagonal secundaria
        // Este caso genera diagonales inferiores a la secundaria
        if(sequenceHasMutation(generateSequense(dna, (dna.length-1), i, -1, 1, 1))){
            countMutation++;        
            if(countMutation >= TOTAL_MINIMAL_MUTATION )
                return countMutation;
        }

        // Este caso genera diagonales superiores a la secundaria
        if(sequenceHasMutation(generateSequense(dna, i-1, 0, -1, 1, 0))){
            countMutation++;        
            if(countMutation >= TOTAL_MINIMAL_MUTATION )
                return countMutation;
        }

        i++;
    }
    
    return countMutation;
}

/**
 * Genera una nueva secuencia con las diagonales de la matriz
 * Verifica si la longitud de la secuencia generada es compatible con
 * la condicion de longitud de secuencia mutada
 * Devuelve la secuencia creada o bien '' si la secuencia no
 * cumple con la longitud minima
 * 
 * @param {Array} dna 
 * @param {Number} row | valor de arranque de la fila
 * @param {Number} column | valor de arranque de la columna
 * @param {Number} sumRow | valor a sumar a la fila, puede ser 1 o -1
 * @param {Number} sumColumn | valor a sumar a la columna puede ser 1 o -1
 * @param {Number} option | Utilizado para condicionar el while, puede ser 1 o 0
 */
function generateSequense(dna, row, column, sumRow, sumColumn, option){
    str = '';

    while((option == 1 && row < dna.length && column < dna.length) || (option == 0 && row >= 0)){
        str += dna[row][column];
        row += sumRow;
        column += sumColumn;
    }

    if(str.length >= LENGTH_STRING){
        return str;
    }
    else {
        return '';
    }
}