# Intrucciones uso servidor mutacion

Desarrollo realizado por Lorena Roxana Izzo

## API

### Agregar mutacion
POST "/mutation/" body: {"dna": Array ['AAAAAA', ..., ]} 
response: 
	200 OK "Con mutacion"
	403 "Sin mutacion"
	403 "Con error"
		  	  
#### Ejemplo peticion POST

```
curl --location --request POST 'localhost:3000/mutation/' \
--header 'Content-Type: application/json' \
--data-raw '{"dna": ["ATGCGA","CTGTGC","TTATGT","AGAAGG","TCCCTA","TCACTG"]}'
```


### Obtener estadisticas
GET "/stats" 
response:
	200 +  {“count_mutations”:40, “count_no_mutation”:100: “ratio”:0.4}
	403 + "Error inesperado"
		   
#### Ejemplo GET

```
curl --location --request GET 'localhost:3000/stats/' \
--header 'Content-Type: application/json'
```	   

	   
## Para correr la aplicacion de manera local:

1.- Requiere redis: se puede utilizar docker con este comando ```docker run --name redis-adn -p 6379:6379 -d redis```

2.- Require nodeJs

### Paso para instalacion
```npm install```

### Paso para ejecucion
```npm start```


## Para consumir el service

URL: http://54.226.144.36

### Ejemplo POST

```
curl --location --request POST 'http://54.226.144.36/mutation/' \
--header 'Content-Type: application/json' \
--data-raw '{"dna": ["ATGCGA","CTGTGC","TTATGT","AGAAGG","TCCCTA","TCACTG"]}'
```

### Ejemplo GET

```
curl --location --request GET 'http://54.226.144.36/stats/' \
--header 'Content-Type: application/json'
```	  



## Test

Para el testeo se utilizo

-Mocha
-Chai
-Nyc

## Para correr el testeo

```
npm test
```

## Para correr el informe de cobertura

```
npm run coverage
```