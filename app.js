const express = require('express');
const redis = require('redis');
var uniqid = require('uniqid');
const { promisify } = require("util");
const hasMutation = require('./hasMutation');
const cors = require('cors');
 
const PORT = process.env.PORT ? process.env.PORT : 3000; 

const app = express();
const client = redis.createClient();

client.on("error", function(error){
    console.log('Error en redis: ',error);
});

const setAsync = promisify(client.set).bind(client);
const keysAsync = promisify(client.keys).bind(client);

app.use(express.json());
app.use(cors());

app.post('/mutation/', async function(req, res){
    try{
        if(!req.body.dna){
            throw new Error("Formato de JSON no valido");
        }

        let dna = req.body.dna.join('');

        if(hasMutation(req.body.dna)){
            await setAsync(uniqid()+'T', dna);
            res.status(200).send("Con mutacion");
            return;
        }
        else{
            await setAsync(uniqid()+'F', dna);
            res.status(403).send("Sin mutacion");
            return;
        }
    }
    catch(e){
        res.status(403).send("Con error");
    }
});


app.get("/stats", async function (req, res){
    try{
        let mutations = (await keysAsync("*T")).length;
        let noMutations = (await keysAsync('*F')).length;
        
        if(noMutations == 0){
            throw new Error("no se puede calcular ratio porque no hay secuencias sin mutacion");
        }

        let response = {
            count_mutations: mutations,
            count_no_mutation: noMutations,
            ratio: (mutations / noMutations)
        }

        res.status(200).send(response);
    }
    catch(e){
        res.status(403).send("Error inesperado" + e);
    }
});


app.listen(PORT, ()=> {
    console.log("Listen in port "+PORT);
});
