const app = require('../app');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = require('chai');

chai.use(chaiHttp);

const PORT = process.env.PORT ? process.env.PORT : 3000;
const serverUrl = `http://localhost:${PORT}`;
const requester =  chai.request.agent(serverUrl);

describe('Peticiones erroneas', () => {
    it('Caracteres no validos', (done) => {
        const data = {"dna":[
            "AVGCGA", // <- V
            "CAGTGC",
            "TTATGT",
            "AGAAGG",
            "CCCCTA",
            "TCACTG"
        ]};
        requester.post('/mutation/').send(data).then(response => {
            expect(response).to.have.status(403);
            done();
        }).catch(e => {
            done(e);
        });
    })

    it('No todos nxm', (done) => {
        const data = {"dna":[
            "AVGCGA",
            "CAGTGC",
            "TTATG", // <-- 1 caracter menos
            "AGAAGG",
            "CCCCTA",
            "TCACTG"
        ]};
        requester.post('/mutation/').send(data).then(response => {
            expect(response).to.have.status(403);
            done();
        }).catch(e => {
            done(e);
        });
    })    
})


describe('Verificar con mutaciones', () => {
    it('Con mutacion 01', (done) => {
        const data = {"dna":[
            "ATGCGA",
            "CAGTGC",
            "TTATGT",
            "AGAAGG",
            "CCCCTA",
            "TCACTG"
        ]};
        requester.post('/mutation/').send(data).then(response => {
            expect(response).to.have.status(200);
            done();
        }).catch(e => {
            done(e);
        });
    })

    it('Con mutacion 02', (done) => {
        const data = {"dna":[
            "ATCCCC",
            "CAGTGC",
            "TTATGT",
            "AGAAGG",
            "CTCCTA",
            "TCACTG"
        ]};
        requester.post('/mutation/').send(data).then(response => {
            expect(response).to.have.status(200);
            done();
        }).catch(e => {
            done(e);
        });
    })   
    
    it('Con mutacion 03', (done) => {
        const data = {"dna":[
            "ATCCGC",
            "CATAGC",
            "TTAGGT",
            "AAGAGG",
            "ATCCTA",
            "TCACTG"
        ]};
        requester.post('/mutation/').send(data).then(response => {
            expect(response).to.have.status(200);
            done();
        }).catch(e => {
            done(e);
        });
    })      
})

//
describe('Check sin mutaciones', () => {
    it('Con mutacion 03', (done) => {
        const data = {"dna":[
            "ATGCGA",
            "CAGTGC",
            "TTATTT",
            "AGACGG",
            "GCGTCA",
            "TCACTG"
        ]};
        requester.post('/mutation/').send(data).then(response => {
            expect(response).to.have.status(403);
            done();
        }).catch(e => {
            done(e);
        });
    })   
})

//
describe('Check /stats', () => {
    it('Respuesta del GET', (done) => {
        requester.get('/stats').send().then(response => {
            expect(response).to.have.status(200);
            done();
        }).catch(e => {
            done(e);
        });
    })   
})
